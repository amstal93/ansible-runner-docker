FROM ansible/ansible-runner:1.4.6

RUN yum update -y && \
    yum install -y yum-utils device-mapper-persistent-data lvm2 which openssh-clients


RUN yum install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm

RUN yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo && \
    yum install -y docker-ce docker-ce-cli
